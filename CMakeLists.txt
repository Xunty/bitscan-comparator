cmake_minimum_required (VERSION 2.8)

PROJECT(Comp_bs)

SET(gtest_dir ${CMAKE_CURRENT_SOURCE_DIR}/deps/google/gtest/src)

#gtests
INCLUDE_DIRECTORIES(./deps/google/gtest/include/gtest)
INCLUDE_DIRECTORIES(./deps)
INCLUDE_DIRECTORIES(./)

ADD_LIBRARY(dep_google_gtest STATIC 
 ${gtest_dir}/gtest-death-test.cc
 ${gtest_dir}/gtest-filepath.cc
 ${gtest_dir}/gtest-port.cc
 ${gtest_dir}/gtest-printers.cc
 ${gtest_dir}/gtest-test-part.cc
 ${gtest_dir}/gtest-typed-test.cc
 ${gtest_dir}/gtest.cc
)

SET(bs_dir ${CMAKE_CURRENT_SOURCE_DIR}/deps/bitstrings)

#bitstring library
SET(SRC_LIST ${bs_dir}/tables.cpp
${bs_dir}/bitboard.cpp
${bs_dir}/bitboardn.cpp
${bs_dir}/bbsentinel.cpp
)


ADD_LIBRARY(bslib ${SRC_LIST}
)
#test target
ADD_EXECUTABLE(tests
./main_test.cpp
./test_bitset.cpp
./test_bitscan.cpp
./test_boost_dynamic_bs.cpp
)

TARGET_LINK_LIBRARIES(tests bslib dep_google_gtest) 