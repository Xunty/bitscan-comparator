#include "gtest.h"
#define N_BITS_ 5000

class InterfaceBitCompGeneral{
public:
	virtual void set();
	virtual void set(int nbit, bool val=true);
	virtual void reset();
	virtual void reset(int nbit);

	virtual bool get_bit(int nbit);

	virtual int count();

	//virtual int next_bit(int nbit);
	//virtual int last_bit(int nbit);


};