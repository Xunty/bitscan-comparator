#include "gtest.h"
#define N_BITS_ 10000
#include "bitstrings/bitscan.h"

class InterfaceBitScan{
public:
	BitBoardN bs;

	void set(){
		bs.set_to_1();
	}
	void set(int nbit, bool val=true){
		if (val)	bs.set_bit(nbit);
		else		bs.erase(nbit);
	}
	void reset(){
		bs.set_to_0();
	}
	void reset(int nbit){
		bs.erase(nbit);
	}

	bool get_bit(int nbit){
		return bs.get_bit(nbit);
	}

	int count(){
		return bs.popcn64();
	}

	int next_bit(int nbit){
		return bs.next_bit(nbit);
	}
	/*int last_bit(int nbit);*/
};

class Test_bitscan: public ::testing::Test{
public:
	InterfaceBitScan b;
	void SetUp(){
		b.bs.init(N_BITS_);
		bool val;
		for(int i=0; i < N_BITS_;i++){
			if (i%2)	val=true;
			else 		val=false;
			b.set(i,val);
		}
	}

};

class Test_bitscan_scan: public ::testing::Test{
public:
	InterfaceBitScan b;
	void SetUp(){
		b.bs.init(N_BITS_);
		bool val;
		for(int i=0; i < N_BITS_;i++){
			if (i%2)	val=true;
			else 		val=false;
			b.set(i,val);
		}
	}

};

TEST_F(Test_bitscan,contador){
	EXPECT_EQ(N_BITS_/2,b.count());
}

TEST_F(Test_bitscan,acceso){
	for(int i=0; i < N_BITS_;i=i+2){
		EXPECT_EQ(false,b.get_bit(i));
	}
}

TEST_F(Test_bitscan_scan,escaneo_nd){
	int obit=0;
	while (true){
		obit = b.next_bit(obit);
		if(obit==EMPTY_ELEM) break;
		EXPECT_EQ(true,b.get_bit(obit));
	}
}

TEST_F(Test_bitscan_scan,escaneo_d){
	int obit=0;
	while (true){
		obit = b.bs.next_bit_if_del(obit);
		if(obit==EMPTY_ELEM) break;
		EXPECT_EQ(true,b.get_bit(obit));
		b.reset(obit);
	}
}
