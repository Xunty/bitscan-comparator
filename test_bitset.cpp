#include "gtest.h"
#define N_BITS_ 10000
#include <bitset>


using namespace std;

class InterfaceBitset{
private:
	bitset<N_BITS_> bs;
public:
	void set(){
		bs.set();
	}
	void set(int nbit, bool val=true){
		bs.set(nbit,val);
	}
	void reset(){
		bs.reset();
	}
	void reset(int nbit){
		bs.reset(nbit);
	}

	bool get_bit(int nbit){
		return bs.test(nbit);
	}

	int count(){
		return bs.count();
	}

	/*int next_bit(int nbit){
		
	}
	int last_bit(int nbit);*/
};

class Test_bitset: public ::testing::Test{
public:
	InterfaceBitset a;
	void SetUp(){
		bool val;
		for(int i=0; i < N_BITS_;i++){
			if (i%2)	val=true;
			else 		val=false;
			a.set(i,val);
		}
	}

};

TEST_F(Test_bitset,contador){
	EXPECT_EQ(N_BITS_/2,a.count());
}

TEST_F(Test_bitset,acceso){
	for(int i=0; i < N_BITS_;i=i+2){
		EXPECT_EQ(false,a.get_bit(i));
	}
}
