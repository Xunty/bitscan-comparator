#include "gtest.h"
#define N_BITS_ 10000
#include "boost/dynamic_bitset/dynamic_bitset.hpp"


class InterfaceBoost{
public:
	boost::dynamic_bitset<> db;

	void set(){
		db.set();
	}
	void set(int nbit, bool val=true){
		db.set(nbit,val);
	}
	void reset(){
		db.reset();
	}
	void reset(int nbit){
		db.reset(nbit);
	}

	bool get_bit(int nbit){
		return db.test(nbit);
	}

	int count(){
		return db.count();
	}

	int next_bit(int nbit){
		return db.find_next(nbit);
	}
	/*int last_bit(int nbit);*/
};

class Test_bitboost: public ::testing::Test{
public:
	InterfaceBoost c;
	void SetUp(){
		c.db.resize(N_BITS_);
		bool val;
		for(int i=0; i < N_BITS_;i++){
			if (i%2)	val=true;
			else 		val=false;
			c.set(i,val);
		}
	}

};

class Test_bitboost_scan: public ::testing::Test{
public:
	InterfaceBoost c;
	void SetUp(){
		c.db.resize(N_BITS_);
		bool val;
		for(int i=0; i < N_BITS_;i++){
			if (i%2)	val=true;
			else 		val=false;
			c.set(i,val);
		}
	}
};

TEST_F(Test_bitboost,contador){
	EXPECT_EQ(N_BITS_/2,c.count());
}

TEST_F(Test_bitboost,acceso){
	for(int i=0; i < N_BITS_;i=i+2){
		EXPECT_EQ(false,c.get_bit(i));
	}
}

TEST_F(Test_bitboost_scan,escaneo_nd){
	int obit=0;
	while (true){
		obit = c.next_bit(obit);
		if(obit==boost::dynamic_bitset<>::npos) break;
		EXPECT_EQ(true,c.get_bit(obit));
	}
}

TEST_F(Test_bitboost_scan,escaneo_d){
	int obit=0;
	while (true){
		obit = c.next_bit(obit);
		if(obit==boost::dynamic_bitset<>::npos) break;
		EXPECT_EQ(true,c.get_bit(obit));
		c.reset(obit);
	}
}